location=$1
cd  /srv/weathermap-grix/grix/; 
/srv/weathermap-python-venv/bin/python3 weathermap.py ${location}; 
cd ../; ./weathermap --config /srv/weathermap-grix/grix/weathermap-${location}.conf --output weathermap-${location}.png --htmloutput weathermap-${location}.html;
cp -r weathermap-${location}.png /srv/ixpmanager6/public/ ; cp -r weathermap-${location}.html /srv/ixpmanager6/public/;
cp -r overlib.js /srv/ixpmanager6/public/
