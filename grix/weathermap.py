import requests
from jinja2 import Environment, FileSystemLoader
import socket
import sys
host=socket.getfqdn()
location=sys.argv[1]
from config import API_KEY
if (location=="ath"):
    switches=[
        {"name":"eie-sw1", "x": 500, "y": 550},
        {"name":"eie-sw2","x": 500, "y": 750},
        {"name":"tis-sp1","x": 400, "y": 100},
        {"name":"tis-sp2","x": 400, "y": 400},
        {"name":"tis-sw1","x": 200, "y": 100},
        {"name":"tis-sw2","x": 200, "y": 400},
        {"name":"tis-sw3","x": 100, "y": 250},
        {"name":"lh-sp1","x": 700, "y": 100},
        {"name":"lh-sp2","x": 700, "y": 400},
        {"name":"lh-sw1","x": 900, "y": 100},
        {"name":"lh-sw2","x": 900, "y": 400},
        {"name":"lh-sw3","x": 1000, "y": 250},
        {"name":"lh-sw4","x": 900, "y": 550},
        ]
elif (location=="thess"):
    switches=[
        {"name":"snc-sw1.gr-ix.gr","x": 500, "y": 100},
        {"name":"snc-sw2.gr-ix.gr","x": 400, "y": 400},
        {"name":"snc-sw3.gr-ix.gr","x": 600, "y": 400},
        ]
else:
    print("Available locations are ath, thess")
    exit()

switches_list = requests.get(url="https://"+str(host)+"/api/v4/provisioner/switch/list.json?apikey="+API_KEY)
switches_list = switches_list.json()
for switch in switches_list["switches"]:
    for switch_static in switches:
        if (switch_static["name"]+".gr-ix.gr" == switch["name"]):
            switch_static["overlibgraph"]="https://"+str(host)+"/grapher/switch?period=day&type=png&category=bits&protocol=all&id="+str(switch["id"])

# Corebundles definition
corebundles = []
corebundles_list = requests.get(url="https://"+str(host)+"/api/v4/provisioner/corebundle/list.json?apikey="+API_KEY)            
corebundles_list = corebundles_list.json()
for corebundle in corebundles_list["corebundles"]:
    identifier=corebundle["id"]
    identifier_for_rrd=0
    if int(identifier)<10:
        identifier_for_rrd="0000"+str(identifier)
    elif int(identifier)<100:
        identifier_for_rrd="000"+str(identifier)

    corebundles.append(
            {"switchsidea":corebundle["switchsidea"], 
             "switchsideb":corebundle["switchsideb"], 
             "bandwidth": corebundle["prettybandwidth"],  
             "overlibgraph":"https://"+str(host)+"/grapher/corebundle?period=day&type=png&category=bits&protocol=all&id="+str(identifier), 
             "target":  "/srv/mrtg/work/corebundles/"+str(identifier_for_rrd)+"/cb-aggregate-"+str(identifier_for_rrd)+"-sidea-bits.rrd:ds0:ds1"
             })


file_loader = FileSystemLoader('./')
env = Environment(loader=file_loader)

template = env.get_template('weathermap.jinja2')

output = template.render(switches=switches,corebundles=corebundles)
with open("weathermap-"+location+".conf", "w") as f:
    f.write(output)
#print(output)
